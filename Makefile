OS := $(shell uname)

start:
ifeq ($(OS),Darwin)
	docker volume create --name=dcsso-sync
	docker-compose -f docker-compose.mac.yml up -d --build
	docker-sync clean
	docker-sync start
	docker exec -it dcsso-php-fpm chown -R www-data:www-data .
else
	docker-compose -f docker-compose.yml up -d --build
	docker exec -it dcsso-php-fpm chown -R www-data:www-data .
endif

stop:
ifeq ($(OS),Darwin)
	docker-compose -f docker-compose.mac.yml stop
	docker-sync stop
else
	docker-compose -f docker-compose.yml stop
endif
	$(info bye-bye!)
